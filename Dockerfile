FROM ubuntu:20.04

# Copie o script para o container
COPY myscript.sh app/myscript.sh

# Torna o script executável
RUN chmod +x app/myscript.sh

# Execute o script quando o container iniciar
CMD ["app/myscript.sh"]
